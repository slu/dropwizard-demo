package net.twonky.demo.dropwizard;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import net.twonky.demo.dropwizard.resources.DropwizardDemoResource;

public class DropwizardDemoApplication extends Application<DropwizardDemoConfiguration> {

    public static void main(String[] args) throws Exception {
        new DropwizardDemoApplication().run(args);
    }

    @Override
    public String getName() {
        return "dropwizard-demo";
    }

    @Override
    public void initialize(Bootstrap<DropwizardDemoConfiguration> bootstrap) {

    }

    @Override
    public void run(DropwizardDemoConfiguration configuration, Environment environment) {

        final DropwizardDemoResource resource = new DropwizardDemoResource(configuration.getDefaultName());
        environment.jersey().register(resource);
    }

}
