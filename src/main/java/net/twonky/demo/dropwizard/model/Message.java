package net.twonky.demo.dropwizard.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Message {

    private String name;

    public Message() {
    }

    public Message(String name) {
        this.name = name;
    }

    @JsonProperty
    public String getName() {
        return name;
    }
}
