package net.twonky.demo.dropwizard.resources;

import net.twonky.demo.dropwizard.model.Message;
import com.google.common.base.Optional;
import com.codahale.metrics.annotation.Timed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/messages")
@Produces(MediaType.APPLICATION_JSON)
public class DropwizardDemoResource {
    private final String defaultName;

    public DropwizardDemoResource(String defaultName) {
        this.defaultName = defaultName;
    }

    @GET
    @Timed
    public Message getMessage(@QueryParam("name") Optional<String> name) {
        final String value = name.or(defaultName);
        return new Message(value);
    }
}
